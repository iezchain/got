#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/currency.hpp>

using namespace eosio;

class got : public eosio::contract {
  public:
      using contract::contract;


       got (account_name self)
      :eosio::contract(self),
      circles(_self, _self),
      circleusers(_self, _self),
      circleprices(_self, _self),
      subscribes(_self, _self),
      replys(_self, _self),
      replydetails(_self, _self)
      {}


    // @abi action
    void createcircle(account_name author, const std::string& name,const std::string& description,account_name publisher,const uint64_t created,const uint64_t status) {
        config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      circles.emplace(author, [&](auto& new_circle) {
        new_circle.id  = id;
        new_circle.name= name;
        new_circle.description= description;
        new_circle.publisher= publisher;
        new_circle.created= created;
        new_circle.status= status;
      });

      store_config(conf);

      eosio::print("circle#", id, " created");
    }

            // @abi action
    void updatecircle(account_name author, const uint64_t id, const std::string& name,const std::string& description,account_name publisher,const uint64_t created,const uint64_t status) {
      auto circle_lookup = circles.find(id);
      eosio_assert(circle_lookup != circles.end(), "circle does not exist");

      circles.modify(circle_lookup, author, [&](auto& modifiable_circle) {
        modifiable_circle.name= name;
        modifiable_circle.description= description;
        modifiable_circle.publisher= publisher;
        modifiable_circle.created= created;
        modifiable_circle.status= status;
   });

  eosio::print("circle#", id, " update");
  }



    // @abi action
  void hdelcircle(account_name author, const uint64_t id) {
    auto circle_lookup = circles.find(id);
    circles.erase(circle_lookup);

    eosio::print("circle#", id, " del");
  }

            // @abi action
 void sdelcircle(account_name author, const uint64_t id) {
      auto circle_lookup = circles.find(id);
      eosio_assert(circle_lookup != circles.end(), "circle does not exist");

      circles.modify(circle_lookup, author, [&](auto& modifiable_circle) {
        modifiable_circle.status= 1;
   });

  eosio::print("circle#", id, " del");
  }



      // @abi action
    void createuser(account_name author, account_name user,std::string nickname,const uint64_t tokens,std::string pubkey,const uint64_t status) {
        config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      circleusers.emplace(author, [&](auto& new_user) {
        new_user.id  = id;
        new_user.user= user;
        new_user.nickname= nickname;
        new_user.tokens= tokens;
        new_user.pubkey= pubkey;
        new_user.status= status;
      });

      store_config(conf);

      eosio::print("user#", id, " created");
    }

                // @abi action
    void updateuser(account_name author, const uint64_t id,account_name user,std::string nickname,const uint64_t tokens,std::string pubkey,const uint64_t status) {
      auto user_lookup = circleusers.find(id);
      eosio_assert(user_lookup != circleusers.end(), "user does not exist");

      circleusers.modify(user_lookup, author, [&](auto& modifiable_user) {
        modifiable_user.user= user;
        modifiable_user.nickname= nickname;
        modifiable_user.tokens= tokens;
        modifiable_user.pubkey= pubkey;
        modifiable_user.status= status;
   });

  eosio::print("user#", id, " update");
  }

      // @abi action
  void hdeluser(account_name author, const uint64_t id) {
    auto user_lookup = circleusers.find(id);
    circleusers.erase(user_lookup);

    eosio::print("user#", id, " del");
  }

              // @abi action
 void sdeluser(account_name author, const uint64_t id) {
      auto user_lookup = circleusers.find(id);
      eosio_assert(user_lookup != circleusers.end(), "user does not exist");

      circleusers.modify(user_lookup, author, [&](auto& modifiable_user) {
        modifiable_user.status= 1;
   });

  eosio::print("user#", id, " del");
  }

      // @abi action
  void createprice(account_name author, const uint64_t circleid,const uint64_t cycle,const uint64_t price,const uint64_t status) {
        config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      circleprices.emplace(author, [&](auto& new_price) {
        new_price.id  = id;
        new_price.circleid= circleid;
        new_price.cycle= cycle;
        new_price.price= price;
        new_price.status= status;
      });

      store_config(conf);

      eosio::print("circleprice#", id, " created");
    }

                // @abi action
    void updateprice(account_name author, const uint64_t id, const uint64_t circleid,const uint64_t cycle,const uint64_t price,const uint64_t status) {
      auto price_lookup = circleprices.find(id);
      eosio_assert(price_lookup != circleprices.end(), "price does not exist");

      circleprices.modify(price_lookup, author, [&](auto& modifiable_price) {
        modifiable_price.circleid= circleid;
        modifiable_price.cycle= cycle;
        modifiable_price.price= price;
        modifiable_price.status= status;
   });

  eosio::print("circleprice#", id, " update");
  }    

        // @abi action
  void hdelprice(account_name author, const uint64_t id) {
    auto price_lookup = circleprices.find(id);
    circleprices.erase(price_lookup);

    eosio::print("circleprice#", id, " del");
  }

              // @abi action
 void sdelprice(account_name author, const uint64_t id) {
      auto price_lookup = circleprices.find(id);
      eosio_assert(price_lookup != circleprices.end(), "price does not exist");

      circleprices.modify(price_lookup, author, [&](auto& modifiable_price) {
        modifiable_price.status= 1;
   });

  eosio::print("circleprice#", id, " del");
  }

      // @abi action
  void createsub(account_name author, account_name subscriber_name,const uint64_t circle_id,std::string circle_name,account_name publisher,const uint64_t cycle,const uint64_t price,const uint64_t subscribe_time,const uint64_t status) {
        config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      subscribes.emplace(author, [&](auto& new_sub) {
        new_sub.id  = id;
        new_sub.subscriber_name= subscriber_name;
        new_sub.circle_id= circle_id;
        new_sub.circle_name= circle_name;
        new_sub.publisher= publisher;
        new_sub.cycle= cycle;
        new_sub.price= price;
        new_sub.subscribe_time= subscribe_time;
        new_sub.status= status;
      });

      store_config(conf);

      eosio::print("subscribe#", id, " created");
    }

 void updatesub(account_name author, const uint64_t id, account_name subscriber_name,const uint64_t circle_id,std::string circle_name,account_name publisher,const uint64_t cycle,const uint64_t price,const uint64_t subscribe_time,const uint64_t status) {
      auto sub_lookup = subscribes.find(id);
      eosio_assert(sub_lookup != subscribes.end(), "sub does not exist");

      subscribes.modify(sub_lookup, author, [&](auto& modifiable_sub) {
        modifiable_sub.subscriber_name= subscriber_name;
        modifiable_sub.circle_id= circle_id;
        modifiable_sub.circle_name= circle_name;
        modifiable_sub.publisher= publisher;
        modifiable_sub.cycle= cycle;
        modifiable_sub.price= price;
        modifiable_sub.subscribe_time= subscribe_time;
        modifiable_sub.status= status;
   });

  eosio::print("sub#", id, " update");
  }  

    // @abi action
  void hdelsub(account_name author, const uint64_t id) {
    auto sub_lookup = subscribes.find(id);
    subscribes.erase(sub_lookup);

    eosio::print("sub#", id, " del");
  } 

    // @abi action
 void sdelsub(account_name author, const uint64_t id) {
      auto sub_lookup = subscribes.find(id);
      eosio_assert(sub_lookup != subscribes.end(), "sub does not exist");

      subscribes.modify(sub_lookup, author, [&](auto& modifiable_sub) {
        modifiable_sub.status= 1;
   });

  eosio::print("sub#", id, " del");
  }
  

   // @abi action
void createreply(account_name author,const uint64_t circle_id,const std::string& circle_name,const std::string& title, account_name publisher,const uint64_t reply_type,const uint64_t pid,const std::string& content,const uint64_t reply_time,const uint64_t status) {

         config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      replys.emplace(author, [&](auto& new_reply) {
        new_reply.id  = id;
        new_reply.circle_id= circle_id;
        new_reply.circle_name= circle_name;
        new_reply.title= title;
        new_reply.publisher= publisher;
        new_reply.reply_type= reply_type;
        new_reply.pid= pid;
        new_reply.content= content;
        new_reply.reply_time= reply_time;
        new_reply.status= status;
      });

      store_config(conf);

      eosio::print("reply#", id, " created");
    }

    // @abi action
    void updatereply(account_name author, const uint32_t id, const uint64_t circle_id,const std::string& circle_name,const std::string& title, account_name publisher,const uint64_t reply_type,const uint64_t pid,const std::string& content,const uint64_t reply_time) {
      auto reply_lookup = replys.find(id);
      eosio_assert(reply_lookup != replys.end(), "reply does not exist");

      replys.modify(reply_lookup, author, [&](auto& modifiable_reply) {
        modifiable_reply.circle_id= circle_id;
        modifiable_reply.circle_name= circle_name;
        modifiable_reply.title= title;
        modifiable_reply.publisher= publisher;
        modifiable_reply.reply_type= reply_type;
        modifiable_reply.pid= pid;
        modifiable_reply.content= content;
        modifiable_reply.reply_time= reply_time;
   });

  eosio::print("reply#", id, " update");
  }

    // @abi action
  void hdelreply(account_name author, const uint32_t id) {
    auto reply_lookup = replys.find(id);
    replys.erase(reply_lookup);

    eosio::print("reply#", id, " del");
  }

   // @abi action
 void sdelreply(account_name author, const uint64_t id) {
      auto reply_lookup = replys.find(id);
      eosio_assert(reply_lookup != replys.end(), "reply does not exist");

      replys.modify(reply_lookup, author, [&](auto& modifiable_reply) {
        modifiable_reply.status= 1;
   });

  eosio::print("reply#", id, " del");
  }

      // @abi action
  void createdetail(account_name author,const uint64_t circle_id,const std::string& circle_name,const std::string& title, account_name publisher,const uint64_t reply_type,const uint64_t pid,const std::string& content,const uint64_t reply_time) {

         config conf;
        get_config(conf);
        uint64_t id = conf.id++;

      replydetails.emplace(author, [&](auto& new_replydetail) {
        new_replydetail.id  = id;
        new_replydetail.circle_id= circle_id;
        new_replydetail.circle_name= circle_name;
        new_replydetail.title= title;
        new_replydetail.publisher= publisher;
        new_replydetail.reply_type= reply_type;
        new_replydetail.pid= pid;
        new_replydetail.content= content;
        new_replydetail.reply_time= reply_time;
      });

      store_config(conf);

      eosio::print("replydetail#", id, " created");
    }

        // @abi action
    void updatedetail(account_name author, const uint32_t id, const uint64_t circle_id,const std::string& circle_name,const std::string& title, account_name publisher,const uint64_t reply_type,const uint64_t pid,const std::string& content,const uint64_t reply_time) {
      auto detail_lookup = replydetails.find(id);
      eosio_assert(detail_lookup != replydetails.end(), "replydetail does not exist");

      replydetails.modify(detail_lookup, author, [&](auto& modifiable_detail) {
        modifiable_detail.circle_id= circle_id;
        modifiable_detail.circle_name= circle_name;
        modifiable_detail.title= title;
        modifiable_detail.publisher= publisher;
        modifiable_detail.reply_type= reply_type;
        modifiable_detail.pid= pid;
        modifiable_detail.content= content;
        modifiable_detail.reply_time= reply_time;
   });

  eosio::print("replydetail#", id, " update");
  }

    // @abi action
  void hdeldetail(account_name author, const uint32_t id) {
    auto detail_lookup = replydetails.find(id);
    replydetails.erase(detail_lookup);

    eosio::print("replydetail#", id, " del");
  }

     // @abi action
 void sdeldetail(account_name author, const uint64_t id) {
      auto detail_lookup = replydetails.find(id);
      eosio_assert(detail_lookup != replydetails.end(), "replydetail does not exist");

      replydetails.modify(detail_lookup, author, [&](auto& modifiable_detail) {
        modifiable_detail.status= 1;
   });

  eosio::print("detail#", id, " del");
  }




 private:    
 
     // @abi table circles i64
    struct circle {
      uint64_t id;
      std::string name;
      std::string description;
      account_name publisher;
      uint64_t created ;
      uint64_t   status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }

      EOSLIB_SERIALIZE(circle, (id)(name)(description)(publisher)(created)(status))
    };

    typedef eosio::multi_index<N(circles), circle> circle_table;
    circle_table circles;
 

      // @abi table circleprices i64
    struct circleprice {
      uint64_t id;
      uint64_t circleid;
      uint64_t cycle;
      uint64_t price;
      uint64_t   status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }

      EOSLIB_SERIALIZE(circleprice, (id)(circleid)(cycle)(price)(status))
    };

    typedef eosio::multi_index<N(circleprices), circleprice> circleprice_table;
    circleprice_table circleprices;

 
     // @abi table circleusers i64
    struct circleuser {
      uint64_t id;
      account_name user;
      std::string nickname;
      uint64_t tokens;
      std::string pubkey;
      uint64_t   status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }
      EOSLIB_SERIALIZE(circleuser, (id)(user)(nickname)(tokens)(pubkey)(status))

    };

    typedef eosio::multi_index<N(circleusers), circleuser> circleuser_table;
    circleuser_table circleusers;
    
    

     // @abi table subscribes i64
    struct subscribe {
      uint64_t id;
      account_name subscriber_name;
      uint64_t circle_id;
      std::string circle_name;
      account_name publisher;
      uint64_t cycle;
      uint64_t price;
      uint64_t subscribe_time;
      uint64_t status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }

      EOSLIB_SERIALIZE(subscribe, (id)(subscriber_name)(circle_id)(circle_name)(publisher)(cycle)(price)(subscribe_time)(status))
    };

    typedef eosio::multi_index<N(subscribes), subscribe> subscribe_table;
    subscribe_table subscribes;

     // @abi table replys i64
    struct reply {
      uint64_t id;
      uint64_t circle_id;
      std::string circle_name;
      std::string title;
      account_name publisher;
      uint64_t reply_type;
      uint64_t pid;
      std::string content;
      uint64_t reply_time;
      uint64_t status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }

      EOSLIB_SERIALIZE(reply, (id)(circle_id)(circle_name)(title)(publisher)(reply_type)(pid)(content)(reply_time)(status))
    };

    typedef eosio::multi_index<N(replys), reply> reply_table;
    reply_table replys;

        // @abi table replydetails i64
    struct replydetail{
      uint64_t id;
      uint64_t circle_id;
      std::string circle_name;
      std::string title;
      account_name publisher;
      uint64_t reply_type;
      uint64_t pid;
      std::string content;
      uint64_t reply_time;
      uint64_t status ;

      //uint64_t primary_key() const { return id; }
      auto primary_key()const { return id; }

      EOSLIB_SERIALIZE(replydetail, (id)(circle_id)(circle_name)(title)(publisher)(reply_type)(pid)(content)(reply_time)(status))
    };

    typedef eosio::multi_index<N(replydetails), replydetail> replydetail_table;
    replydetail_table replydetails;
    
    
    static uint64_t id;

    struct config
    {
        config() {}
        constexpr static uint64_t key = N(config);
        uint64_t id = 1;
    };

    void store_config(const config &conf)
    {
        auto it = db_find_i64(_self, _self, N(config), config::key);
        if (it != -1)
        {
            db_update_i64(it, _self, (const char *)&conf, sizeof(config));
        }
        else
        {
            db_store_i64(_self, N(config), _self, config::key, (const char *)&conf, sizeof(config));
        }
    }

    bool get_config(config &conf)
    {
        auto it = db_find_i64(_self, _self, N(config), config::key);
        if (it != -1)
        {
            auto size = db_get_i64(it, (char *)&conf, sizeof(config));
            eosio_assert(size == sizeof(config), "Wrong record size");
            return true;
        }
        return false;
    }






};


EOSIO_ABI( got, (hi)(createcircle)(updatecircle)(hdelcircle)(sdelcircle)(createuser)(updateuser)(hdeluser)(sdeluser)(createprice)(updateprice)(hdelprice)(sdelprice)(createsub)(updatesub)(hdelsub)(sdelsub)(createreply)(updatereply)(hdelreply)(sdelreply)(createdetail)(updatedetail)(hdeldetail)(sdeldetail) )
