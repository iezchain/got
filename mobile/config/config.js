export default {
    config: {
        chainId: '706a7ddd808de9fc2b8879904f3b392256c83104c1d544b38302cc07d9fca477', // 32 byte (64 char) hex string
        keyProvider: ["5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3","5KE9TwBrypQ9ouY5PorKrLhNdXkEMNhKGvV2KMWqPuB3GNRbusH"], // WIF string or array of keys..   t1 private key
        httpEndpoint: 'http://c2.iezsoft.com',
        mockTransactions: () => null, // or 'fail'
        // transactionHeaders: (expireInSeconds, callback) => {
        //   callback(null/*error*/, headers)
        // },
        expireInSeconds: 60,
        broadcast: true,
        debug: false,
        sign: true
    },
    userConfig: {
        scope: '',
        code: 'gotuser',
        privateKey: '',
        table: [
            {
                tableName: "circles"
            },
            {
                tableName: "circleusers"
            },
            {
                tableName: "replys"
            },
            {
                tableName: "replydetails"
            },
            {
                tableName: "subscribes"
            },
            {
                tableName: "circleprices"
            }
        ]
    }


    // publicKey: this.publicKey
}