const tab_class = () =>
    import ( /* webpackChunkName: "tabbar-class" */ '@/views/user/index.vue');

const Tabbar = () =>
    import ( /* webpackChunkName: "Tabbar" */ '@/vue/components/Tabbar/');

export default [{
    path: "/user",
    name: "user",
    meta: {
        keepAlive: false
    },
    components: { default: tab_class, tabbar: Tabbar }
}]