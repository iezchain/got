const tab_class = () =>
    import ('@/views/planet/index.vue');
const PlanetList = () =>
    import ('@/views/planet/list/');
const PlanetPublish = () =>
    import ('@/views/planet/publish/');
const PlanetDetail = () =>
    import ('@/views/planet/detail/');
const PlanetCreate = () =>
    import ('@/views/planet/createPlanet/');
const JoinPlanet = () =>
    import ('@/views/planet/joinPlanet/');

const Tabbar = () =>
    import ('@/vue/components/Tabbar/');

export default [{
    path: "planet/",
    name: "planet",
    meta: {
        keepAlive: false
    },
    components: { default: tab_class, tabbar: Tabbar }
}, {
    path: "/planet/detail",
    name: "detail",
    meta: {
        keepAlive: false
    },
    component: PlanetDetail
}, {
    path: "/planet/createPlanet",
    name: "createPlanet",
    meta: {
        keepAlive: false
    },
    component: PlanetCreate
}, {
    path: "/planet/joinPlanet",
    name: "joinPlanet",
    meta: {
        keepAlive: false
    },
    component: JoinPlanet
}, {
    path: "/planet/publish",
    name: "publish",
    meta: {
        keepAlive: false
    },
    component: PlanetPublish
}, {
    path: "/planet/list",
    name: "list",
    meta: {
        keepAlive: false
    },
    component: PlanetList
}]