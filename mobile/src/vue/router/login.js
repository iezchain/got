const login = () =>
    import ( /* webpackChunkName: "login" */ '@/views/login/login');
const settingConfig = () =>
    import ( /* webpackChunkName: "register-getCode" */ '@/views/login/setting_config/');
const registerSubmit = () =>
    import ( /* webpackChunkName: "register-submit" */ '@/views/login/register-submit/');
const registerStatus = () =>
    import ( /* webpackChunkName: "register-status" */ '@/views/login/register-status/');


export default [{
    path: "/",
    name: "login",
    meta: {
        keepAlive: false
    },
    component: login
}, {
    path: "/login/settingConfig",
    name: "settingConfig",
    meta: {
        keepAlive: false
    },
    component: settingConfig
}, {
    path: "/login/registerSubmit",
    name: "registerSubmit",
    meta: {
        keepAlive: false
    },
    component: registerSubmit
}, {
    path: "/login/registerStatus/:status",
    name: "registerStatus",
    props: true,
    component: registerStatus
}, {
    path: '*',
    redirect: {
        name: 'login'
    }
}]