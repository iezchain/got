import Vue from 'vue';
import Router from 'vue-router';

import planet from './planet';
import login from './login';
import user from './user';

Vue.use(Router);

let RouterModel = new Router({
	routes:[...planet, ...login,...user]
})


RouterModel.beforeEach((to, from, next) => {
	const { Authorization, user_id} = Vue.prototype.$util.getLocalStorage('Authorization', 'user_id')
	if(!Authorization && !user_id){
		if(to.meta.login){
			RouterModel.push({name: 'login', query: {redirect: to.name}});
			return;
		}
	}
	next();
})

RouterModel.afterEach((to, from) => {
})

export default RouterModel;