import '@/assets/scss/iconfont/iconfont.css';
import Vue from 'vue';
import App from './App.vue';
import router from '@/vue/router/';
import Vuelidation from '@/vue/plugins/vuelidation';
import VueCountdown from '@/vue/plugins/vue-countdown';

import 'babel-polyfill';

import {
	Waterfall,
	Lazyload,
	Toast,
	Tag,
	Dialog,
	Cell,
	CellGroup,
	Field,
	Icon,
	Button,
	Popup,
	loading,
	List
} from 'vant';

import util from '@/assets/js/util';
import filters from "@/vue/filter/";

import defines from '../config/config';

Vue.prototype.defines = defines;

Vue.prototype.changeConfig = function (config){//changeConfig
	Vue.prototype.defines = config;
	console.log('123');
  }

// plugins
Vue.use(VueCountdown);
Vue.use(Vuelidation);
// vue
Vue.use(filters);
Vue.use(util);

// vant
Vue.use(Waterfall);
Vue.use(Lazyload, {
	preLoad: 1.3,
	error: '/static/img/goods_default.png',
	loading: '/static/img/goods_default.png',
	attempt: 1,
	listenEvents: ['scroll'],
	lazyComponent: true,
});

Vue.use(Tag);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Field);
Vue.use(Icon);
Vue.use(Button);
Vue.use(Popup);
Vue.use(loading);
Vue.use(List);
Toast.setDefaultOptions({
	duration: 1500
})
//FastClick.attach(document.body);
new Vue({
	el: '#app',
	router,
	render: h => h(App)
});
