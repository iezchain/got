package web
import (
	"server/store"
	"github.com/widuu/goini"
	"fmt"
	"github.com/eoscanada/eos-go"
)





var db *store.DataStore

func SetStore(store *store.DataStore){
	db=store
}


func GetTable() string{
	//读取配置文件
	conf := goini.SetConfig("config.ini")
	url := conf.GetValue("conf", "url")
	tablename :=conf.GetValue("conf", "tablename")
	username :=conf.GetValue("conf", "username")
	fmt.Println("url:",url)
	fmt.Println("tablename:",tablename)
	fmt.Println("username:",username)


	//取表数据
	//api := eos.New("https://c2.iezsoft.com")
	api := eos.New(url)

	api.Debug = true
	eos.Debug = true

	//infoResp, _ := api.GetInfo()
	//accountResp, _ := api.GetAccount("t2")
	//fmt.Println("Permission for initn:", accountResp.Permissions[0].RequiredAuth.Keys)
	//fmt.Println("Getinfo",infoResp)
	var request eos.GetTableRowsRequest
	request.Scope=username
	request.Code=username
	request.JSON=true
	request.Table=tablename
	resp,_:=api.GetTableRows(request)
	return string(resp.Rows)

}

