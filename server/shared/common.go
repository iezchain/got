package shared

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/csv"
	"encoding/hex"
	"encoding/json"
	"errors"
	"hash"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	HASH_SHA1        = iota
	HASH_SHA256
	HASH_SHA512
	HASH_SHA512_384
	SATOSHIS_PER_BTC = 100000000
	SATOSHIS_PER_LTC = 100000000
	BTC_USD          = "btc_usd"
	TIMESPAN_1MIN    = "1min"
	TIMESPAN_3MIN    = "3min"
	TIMESPAN_5MIN    = "5min"
	TIMESPAN_15MIN   = "15min"
	TIMESPAN_30MIN   = "30min"
	TIMESPAN_1HOUR   = "1hour"
	TIMESPAN_2HOUR   = "2hour"
	TIMESPAN_4HOUR   = "4hour"
	TIMESPAN_6HOUR   = "6hour"
	TIMESPAN_12HOUR  = "12hour"
	TIMESPAN_1DAY    = "1day"
	TIMESPAN_3DAY    = "3day"
	TIMESPAN_1WEEK   = "1week"
)

func GetMD5(input []byte) []byte {
	hash := md5.New()
	hash.Write(input)
	return hash.Sum(nil)
}

func GetSHA512(input []byte) []byte {
	sha := sha512.New()
	sha.Write(input)
	return sha.Sum(nil)
}

func GetSHA256(input []byte) []byte {
	sha := sha256.New()
	sha.Write(input)
	return sha.Sum(nil)
}

func GetHMAC(hashType int, input, key []byte) []byte {
	var hash func() hash.Hash

	switch hashType {
	case HASH_SHA1:
		{
			hash = sha1.New
		}
	case HASH_SHA256:
		{
			hash = sha256.New
		}
	case HASH_SHA512:
		{
			hash = sha512.New
		}
	case HASH_SHA512_384:
		{
			hash = sha512.New384
		}
	}

	hmac := hmac.New(hash, []byte(key))
	hmac.Write(input)
	return hmac.Sum(nil)
}

func HexEncodeToString(input []byte) string {
	return hex.EncodeToString(input)
}

func Base64Decode(input string) ([]byte, error) {
	result, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func Base64Encode(input []byte) string {
	return base64.StdEncoding.EncodeToString(input)
}

func StringSliceDifference(slice1 []string, slice2 []string) []string {
	var diff []string
	for i := 0; i < 2; i++ {
		for _, s1 := range slice1 {
			found := false
			for _, s2 := range slice2 {
				if s1 == s2 {
					found = true
					break
				}
			}
			if !found {
				diff = append(diff, s1)
			}
		}
		if i == 0 {
			slice1, slice2 = slice2, slice1
		}
	}
	return diff
}

func StringContains(input, substring string) bool {
	return strings.Contains(input, substring)
}

func JoinStrings(input []string, seperator string) string {
	return strings.Join(input, seperator)
}

func SplitStrings(input, seperator string) []string {
	return strings.Split(input, seperator)
}

func TrimString(input, cutset string) string {
	return strings.Trim(input, cutset)
}

func StringToUpper(input string) string {
	return strings.ToUpper(input)
}

func StringToLower(input string) string {
	return strings.ToLower(input)
}

func RoundFloat(x float64, prec int) float64 {
	var rounder float64
	pow := math.Pow(10, float64(prec))
	intermed := x * pow
	_, frac := math.Modf(intermed)
	intermed += .5
	x = .5
	if frac < 0.0 {
		x = -.5
		intermed -= 1
	}
	if frac >= x {
		rounder = math.Ceil(intermed)
	} else {
		rounder = math.Floor(intermed)
	}

	return rounder / pow
}

func IsEnabled(isEnabled bool) string {
	if isEnabled {
		return "Enabled"
	} else {
		return "Disabled"
	}
}

func CalculateAmountWithFee(amount, fee float64) float64 {
	return amount + CalculateFee(amount, fee)
}

func CalculateFee(amount, fee float64) float64 {
	return amount * (fee / 100)
}

func CalculatePercentageDifference(amount, secondAmount float64) float64 {
	return (secondAmount - amount) / amount * 100
}

func CalculateNetProfit(amount, priceThen, priceNow, costs float64) float64 {
	return (priceNow * amount) - (priceThen * amount) - costs
}

func SendHTTPRequest(method, path string, headers map[string]string, body io.Reader) (string, error) {
	result := strings.ToUpper(method)

	if result != "POST" && result != "GET" && result != "DELETE" {
		return "", errors.New("Invalid HTTP method specified.")
	}

	req, err := http.NewRequest(method, path, body)

	if err != nil {
		return "", err
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	httpClient := &http.Client{}
	resp, err := httpClient.Do(req)

	if err != nil {
		return "", err
	}

	contents, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		return "", err
	}

	return string(contents), nil
}

func SendHTTPGetRequest(url string, jsonDecode bool, result interface{}) (err error) {
	res, err := http.Get(url)

	if err != nil {
		return err
	}

	if res.StatusCode != 200 {
		log.Printf("HTTP status code: %d\n", res.StatusCode)
		return errors.New("Status code was not 200.")
	}

	contents, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return err
	}

	defer res.Body.Close()

	if jsonDecode {
		err := JSONDecode(contents, &result)

		if err != nil {
			return err
		}
	} else {
		result = &contents
	}

	return nil
}

func JSONEncode(v interface{}) ([]byte, error) {
	json, err := json.Marshal(&v)

	if err != nil {
		return nil, err
	}

	return json, nil
}

func JSONDecode(data []byte, to interface{}) error {
	err := json.Unmarshal(data, &to)

	if err != nil {
		return err
	}

	return nil
}

func EncodeURLValues(url string, values url.Values) string {
	path := url
	if len(values) > 0 {
		path += "?" + values.Encode()
	}
	return path
}

func ExtractHost(address string) string {
	host := SplitStrings(address, ":")[0]
	if host == "" {
		return "localhost"
	}
	return host
}

func ExtractPort(host string) int {
	portStr := SplitStrings(host, ":")[1]
	port, _ := strconv.Atoi(portStr)
	return port
}

func OutputCSV(path string, data [][]string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(file)

	err = writer.WriteAll(data)
	if err != nil {
		return err
	}

	defer writer.Flush()
	return nil
}

func GetMilliSpanOfKlineType(klineType string) int64 {
	var timeSpan int64
	switch klineType {
	case TIMESPAN_1MIN:
		timeSpan = 1000 * 60
	case TIMESPAN_3MIN:
		timeSpan = 3 * 1000 * 60
	case TIMESPAN_5MIN:
		timeSpan = 5 * 1000 * 60
	case TIMESPAN_15MIN:
		timeSpan = 15 * 1000 * 60
	case TIMESPAN_30MIN:
		timeSpan = 30 * 1000 * 60
	case TIMESPAN_1HOUR:
		timeSpan = 60 * 1000 * 60
	case TIMESPAN_2HOUR:
		timeSpan = 120 * 1000 * 60
	case TIMESPAN_4HOUR:
		timeSpan = 240 * 1000 * 60
	case TIMESPAN_6HOUR:
		timeSpan = 360 * 1000 * 60
	case TIMESPAN_12HOUR:
		timeSpan = 720 * 1000 * 60
	case TIMESPAN_1DAY:
		timeSpan = 1440 * 1000 * 60
	case TIMESPAN_3DAY:
		timeSpan = 4320 * 1000 * 60
	case TIMESPAN_1WEEK:
		timeSpan = 10080 * 1000 * 60
	default:
		timeSpan =0
	}
	return timeSpan
}

func GetLocalDate(timeStamp int64) string {
    return time.Unix(timeStamp,0).Format("2006-01-02 15:04:05");
}