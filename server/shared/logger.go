package shared

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"gopkg.in/natefinch/lumberjack.v2"
)

var Logger *zap.SugaredLogger

func  InitialDefalutLogger()  {
	var productLogger , _ = zap.NewProduction()
	Logger =productLogger.Sugar();
}
func ConfigLogger(logFile string,maxSize,maxBackup,maxAge int) {
	highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel
	})



	// High-priority output should also go to standard error, and low-priority
	// output should also go to standard out.
	consoleDebugging := zapcore.Lock(os.Stdout)
	consoleErrors := zapcore.Lock(os.Stderr)


	consoleEncoder := zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig())


	w := zapcore.AddSync(&lumberjack.Logger{
		Filename: logFile,
		MaxSize:    maxSize, // megabytes
		MaxBackups: maxBackup,
		MaxAge:     maxAge, // days
	})
	rollCore := zapcore.NewCore(
		zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
		w,
		zap.InfoLevel,
	)

	// Join the outputs, encoders, and level-handling functions into
	// zapcore.Cores, then tee the four cores together.
	core := zapcore.NewTee(
		zapcore.NewCore(consoleEncoder, consoleErrors, highPriority),
		zapcore.NewCore(consoleEncoder, consoleDebugging, lowPriority),
		rollCore,
	)

	// From a zapcore.Core, it's easy to construct a Logger.
	Logger = zap.New(core).Sugar()
	defer Logger.Sync()
	Logger.Info("constructed a Logger")
}