package shared

import (
	"io/ioutil"
	"encoding/json"
	"os"
	"path"
)

const (
	CONFIG_FILE = "conf/config.json"
)


var Root string



type Config struct {
	Env        string `json:"env"`
	Key           string `json:"yun_key"`
	Secret            string `json:"yun_secret"`
	DataSource string `json:"dataSource"`
}



func ReadConfig() (Config, error) {
	var err error
	Root,err=os.Getwd();
	configFile:=path.Join(Root,CONFIG_FILE);
	file, err := ioutil.ReadFile(configFile)

	if err != nil {
		return Config{}, err
	}

	cfg := Config{}
	err = json.Unmarshal(file, &cfg)
	return cfg, err
}
