package store
import (
	_ "github.com/mattn/go-sqlite3"
	"github.com/go-xorm/xorm"
	_"github.com/go-xorm/core"
)


type DataStore struct {
	driver string
	dataSource string
	dbEngine *xorm.Engine
}


func InitStore(driver,ds string) (*DataStore,error)  {
	engine,err:=xorm.NewEngine(driver,ds);
	if(err!=nil){
		return  nil,err
	}
	store:=&DataStore{driver,ds,engine}
	err=engine.Ping()
	return  store,err
}



