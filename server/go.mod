module server

require (
	github.com/eoscanada/eos-go v0.0.0-20180604144740-f03a3441b142
	github.com/gin-gonic/gin v1.1.4
	github.com/go-sql-driver/mysql v1.4.0
	github.com/go-xorm/core v0.6.0
	github.com/go-xorm/xorm v0.7.0
	github.com/mattn/go-isatty v0.0.3
	github.com/mattn/go-sqlite3 v1.7.0
	github.com/widuu/goini v0.0.0-20180603013956-56a38bd2e09b
	go.uber.org/zap v1.8.0
	golang.org/x/crypto v0.0.0-20180608092829-8ac0e0d97ce4
	gopkg.in/natefinch/lumberjack.v2 v2.0.0-20170531160350-a96e63847dc3
	gopkg.in/yaml.v2 v2.2.1
)
