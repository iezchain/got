package main

import (
	"server/store"
	"server/shared"
	"server/web"
	"os"
	"github.com/gin-gonic/gin"
)

func main() {

	shared.InitialDefalutLogger()
	config, err := shared.ReadConfig()
	if err == nil {
		shared.Logger.Info(config)
	} else {
		shared.Logger.Info("读取配置文件出错")
		os.Exit(1)
	}
	logFile:=shared.Root+"/log/api.log"
	shared.ConfigLogger(logFile,5,1000,30)

	db,err:=store.InitStore("SQLite",config.DataSource)
	web.SetStore(db)
	r := gin.Default()
	v1 := r.Group("/api/v1")
    println("ok")
	v1.GET("/ping", func(c *gin.Context) {
		println("gettable")
		s:=web.GetTable()
		c.JSON(200, gin.H{
			"message": s,
		})
	})
	s:=web.GetTable()
	println(s)
	r.Run(":8080")
}